// Day 3 No matter how you slice it, calculate home square inches of fabric between two or more claim
// Ex: claim : #123 @ 3,2: 5x4 .... ID: 123,
// 		Rectangle 3 inches from left edge, 2 inches from top edge
// 		5 inches wide, 4 inches tall

package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
	"time"
)

type claim struct {
	id     string
	fledge string
	ftedge string
	wide   string
	tall   string
}

const sz = 1000

func main() {
	t1 := time.Now()
	f, err := os.Open("/home/armando/go/Proj/goAdventCode/day3/day3input.txt")
	if err != nil {
		log.Fatal("%q.dude....you know better.", err)
	}
	defer f.Close()
	land := &[sz][sz]string{}
	claims := &[]claim{}
	scanner := bufio.NewScanner(f)
	initLand(land)
	c := claim{}
	for scanner.Scan() {
		c = parseClaim(scanner.Text())
		*claims = append(*claims, c)
	}
	drawClaim(claims, land)
	printClaims(*land)
	fmt.Printf("Sqrt ft share: %d\n", calcSqrtInch(*land))
	c = noOverlap(*land, claims)
	fmt.Printf("No overlap claim ID: %s\n", c.id)
	t2 := time.Now()
	fmt.Printf("It took %v to complete that process\n", t2.Sub(t1))
}

func printClaims(l [sz][sz]string) {
	for i := 0; i < 100; i++ {
		fmt.Println(strings.Join(l[i][:], ""))
	}
}

func parseClaim(s string) claim {
	c := claim{}
	id := false
	at := false
	dim := false
	inBetween := false
	for _, v := range s {
		switch string(v) {
		case "#":
			id = true
			at = false
			dim = false
			continue
		case "@":
			id = false
			at = true
			dim = false
			continue
		case ":":
			id = false
			at = false
			dim = true
			continue
		case ",", "x":
			inBetween = true
			continue
		case " ":
			inBetween = false
			continue
		}

		if id && !at && !dim {
			c.id += string(v)
			continue
		}
		if at && !id && !dim {
			if inBetween {
				//top edge
				c.ftedge += string(v)
				continue
			}
			c.fledge += string(v)
			continue
		}
		if dim && !id && !at {
			if inBetween {
				c.tall += string(v)
				continue
			}
			c.wide += string(v)
		}
	}
	return c
}

func drawClaim(claims *[]claim, land *[sz][sz]string) {
	for _, c := range *claims {
		drawItOut(c, land)
	}
}

func drawItOut(c claim, l *[sz][sz]string) {
	le, te, t, w := claimIntProp(c)
	for i := 0; i < sz; i++ {
		for j := 0; j < sz; j++ {
			if j == le && i == te {
				for x := i; x < t+i; x++ {
					for y := j; y < w+j; y++ {
						if (*l)[x][y] == "*" {
							(*l)[x][y] = c.id
						} else {
							(*l)[x][y] = "X"
						}
					}
				}
				break
			}
		}
	}
}

func calcSqrtInch(l [sz][sz]string) int {
	var x int
	for i := 0; i < sz; i++ {
		for j := 0; j < sz; j++ {
			if l[i][j] == "X" {
				x++
			}
		}
	}
	return x
}

func initLand(l *[sz][sz]string) {
	for i := 0; i < sz; i++ {
		for j := 0; j < sz; j++ {
			(*l)[i][j] = "*"
		}
	}
}

func noOverlap(l [sz][sz]string, cls *[]claim) claim {
	var le, te, t, w int
	noX := true
	for _, c := range *cls {
		le, te, t, w = claimIntProp(c)
		noX = helper(le, te, t, w, l, c)
		if noX {
			return c
		}
	}
	return claim{}
}

//this is Not pretty
func helper(le, te, t, w int, l [sz][sz]string, c claim) bool {
	for y := 0; y < sz; y++ {
		for x := 0; x < sz; x++ {
			if y == te && x == le {
				for i := y; i < t+y; i++ {
					for j := x; j < w+x; j++ {
						if l[i][j] != c.id {
							return false
						}
					}
				}
			}

		}
	}
	return true
}

func claimIntProp(c claim) (leftEdge, topEdge, tall, wide int) {
	leftEdge, err := strconv.Atoi(c.fledge)
	if err != nil {
		log.Fatal(err)
	}
	topEdge, err = strconv.Atoi(c.ftedge)
	if err != nil {
		log.Fatal(err)
	}
	tall, err = strconv.Atoi(c.tall)
	if err != nil {
		log.Fatal(err)
	}
	wide, err = strconv.Atoi(c.wide)
	if err != nil {
		log.Fatal(err)
	}
	return leftEdge, topEdge, tall, wide
}
