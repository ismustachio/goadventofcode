// Day 1Chronal Calibration
// Ex; f {+1,-2,+3,+1} ; startFreq = 0; result f = 3

package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"strconv"
	"strings"
)

var filename = flag.String("f", "", "File containing f's..should be in same dir")

func main() {
	flag.Parse()
	if *filename == "" {
		flag.Usage()
		fmt.Printf("%s\n", *filename)
		os.Exit(1)
	}
	freqs, err := parseFreq(*filename)
	if err != nil {
		fmt.Printf("%q\n", err)
		os.Exit(1)
	}
	dupfreq := calibrateFreq(freqs)
	fmt.Printf("First duplicate frequency: %d\n", dupfreq)
}

func parseFreq(s string) ([]int, error) {
	var freqslice []int
	file, err := os.Open(s)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		freqslice = append(freqslice, handleNum(scanner.Text()))
	}
	if err = scanner.Err(); err != nil {
		return nil, err
	}
	return freqslice, nil
}

func handleNum(s string) int {
	if strings.Contains(s, "+") {
		sl := strings.SplitAfter(s, "+")
		n, err := strconv.Atoi(strings.Join(sl, ""))
		if err != nil {
			fmt.Printf("%q\n", err)
			os.Exit(1)
		}
		return n
	} // assuming is always pos and negative

	sl := strings.SplitAfter(s, "-")
	n, err := strconv.Atoi(strings.Join(sl, ""))
	if err != nil {
		fmt.Printf("%q\n", err)
		os.Exit(1)
	}

	return n
}

func calibrateFreq(f []int) int {
	result := int(0)
	seen := make(map[int]struct{}, len(f))
	seen[result] = struct{}{}
	for _, v := range f {
		fmt.Printf("Current frequency: %d, change of %d, resulting in %d\n",
			result, v, result+v)
		result += v
		seen[result] = struct{}{}
	}
	for {
		for _, v := range f {
			result += v
			if _, ok := seen[result]; ok {
				return result
			}

		}
	}
	return 0
}