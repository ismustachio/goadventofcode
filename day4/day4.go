package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
	"time"
)

type GuardsRecords struct {
	Id      string
	Year    string
	Mnth    string
	Day     string
	Minute  string
	Hour    string
	Message string
	Asleep  int
	Awake   int
}

func main() {
	t0 := time.Now()
	f, err := os.Open("")
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	var dt, tm, msg string
	gr := []GuardsRecords{}
	g := &GuardsRecords{}
	scanner := bufio.NewScanner(f)
	m := make(map[string]GuardsRecords)
	for scanner.Scan() {
		dt, tm, msg = splitRecord(scanner.Text())
		handleDate(dt, g)
		splitTime(tm, g)
		handleMsg(msg, g)
		gr = append(gr, *g)
		m[(*g).Id] = *g
		g = &GuardsRecords{}
	}
	// LOOP THRU THE RECORDS AND UPDATE AS IT GO THE MINUTES ASLEEP AND AWAKE
	fmt.Println(len(gr))
	t1 := time.Now()
	fmt.Println(t1.Sub(t0))
}

func splitRecord(s string) (string, string, string) {
	a := strings.Split(s, "]")
	b := strings.Split(strings.Join(a, ""), "[")
	c := strings.Split(strings.Join(b, ""), " ")
	//should be split into date, time, and ... rest is the message
	return strings.Join(c[:1], ""), strings.Join(c[1:2], ""), strings.Join(c[2:], "")
}

func splitTime(s string, g *GuardsRecords) {
	a := strings.Split(s, ":")
	(*g).Hour = strings.Join(a[:1], "")
	(*g).Minute = strings.Join(a[1:], "")
}

func handleDate(s string, g *GuardsRecords) {
	a := strings.SplitAfter(s, "-")
	b := strings.Split(strings.Join(a, ""), "-")
	(*g).Year = strings.Join(b[:1], "")
	(*g).Mnth = strings.Join(b[1:2], "")
	(*g).Day = strings.Join(b[2:], "")
}

func handleMsg(s string, g *GuardsRecords) {
	a := strings.SplitAfter(s, "#")
	for _, v := range a[1:] {
		for _, x := range v {
			if _, err := strconv.Atoi(string(x)); err != nil {
				(*g).Message += string(x)
				continue
			}
			(*g).Id += string(x)
		}
	}
}
