//counting the number that have an ID containing exactly two of any letter and then separately countin//those with exactly three of any letter.
// Box ids: "abcdef", "bababc", "abbcde", "abcccd", "aabcdd", "abcdee", "ababab"}
// Result checksum: 4 * 3 = 12

package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
)

var filename = flag.String("f", "", "day2input.txt.\n ")

func main() {
	flag.Parse()
	if len(os.Args[1:]) == 0 {
		flag.Usage()
		os.Exit(1)
	}

	sl := []string{}
	if *filename != "" {
		file, err := os.Open(*filename)
		if err != nil {
			fmt.Printf("%q\n", err)
			os.Exit(1)
		}
		defer file.Close()
		scanner := bufio.NewScanner(file)
		for scanner.Scan() {
			sl = append(sl, scanner.Text())
		}
		var th, tw bool
		var two, three int
		func(s []string) {
			for _, v := range s {
				tw, th = countEm(v)
				if tw {
					two++
				}
				if th {
					three++
				}
			}
		}(sl)

		offBYone(sl)
		fmt.Printf("Checksum: %d\n", (two * three))

	}
}

func offBYone(sl []string) {
	simSL := []string{}
	sim := ""
	for _, s := range sl {

		//going in
		for _, v := range sl {
			if s == v {
				continue
			}
			sim = checkIt(s, v)
			if sim != "" {
				simSL = append(simSL, sim)
			}

		}
	}
	fmt.Printf("Letters common between correct Box ID's\n")
	for _, v := range simSL {
		fmt.Println(v)
	}
}

func checkIt(s1, s2 string) string {
	m := min(s1, s2)
	count := int(0)
	sim := ""
	for i := 0; i < m; i++ {
		if s1[i] != s2[i] {
			count++
		} else {
			sim += string(s1[i])
		}
	}
	if count == 1 {
		return sim
	}
	return ""
}

func min(s1, s2 string) int {
	//already checked if equal before called
	if len(s1) > len(s2) {
		return len(s2)
	} 
	return len(s1)
}

func countEm(s string) (two, three bool) {
	fmt.Printf("%s\n", s)
	ch := make(map[rune]int)
	for _, v := range s {
		ch[v]++
	}
	for _, v := range ch {
		if v >= 3 {
			three = true
		}
		if v == 2 {
			two = true
		}
	}
	return two, three
}